//alert("Hello");

//ARITHMETIC OPERATORS

let x = 1397;
let y = 7831;

//Addition Operator

let sum = x + y;
console.log("Result of addition operator: " + sum);

//Subtraction Operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

//Multiplication Operator
let product = x * y;
console.log("Result of multiplication operator: " + product);

//Division Operator
let quotient = x / y;
console.log("Result of division operator: " + quotient);

//Modulo Operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


//ASSIGNMENT OPERATOR
//Basic Assignment Operator (=)
//The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

//Addition Assignment Operator
//Uses the current value of the variable and it adds a number (2) to itself. Afterwards, reassigns it as a new value.
//assignmentNumber = assignmentNumber + 2
assignmentNumber += 2;
console.log("Result of the addition assignment operator " + assignmentNumber);

//Subtraction/Multiplication/Division (-=, *=, /=)
//Subtraction Assignment Operator
assignmentNumber -= 2; //assignmentNumber = assignmentNumber - 2
console.log("Result of subtraction assignment operator: " + assignmentNumber);

//Multiplication Assignment Operator
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

//Division Assignment Operator
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

//Multiple Operators and Parentheses

/*
	When multiple operators are applied in a single statement, it follows the PEMDAS RULE (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);



//INCREMENT AND DECREMENT
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.

let z = 1;

//Pre-Increment
//The value of "z" is added by a value of 1 before returning the value and storing
/*let preIncrement = ++z;
console.log("Result of pre-increment: : " + preIncrement);
console.log("Result of pre-increment: " + z);*/

//Post Increment
//The value of "z" returned and stored in the variable "postIncrement" then the value of "z" is increased by one.
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the post-increment: " + z);

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1
	Pre-decrement - subtracts 1 first before reading value
	Post-decrement - reads value first before subtracting 1
*/

let a = 2;
//Pre-Decrement
//The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement"
/*let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement); //1
console.log("Result of the pre-decrement: " + a); //1
*/

//Post-Decrement
let postDecrement = a--;
//The value of "a" is returned and stored in the variable "postDecrement the the value of "a"
console.log("Result of the pre-decrement: " + postDecrement); //2
console.log("Result of the pre-decrement: " + a); //1


//TYPE COERCION

/*
	Type coercion is automatic or implicit conversion of values from one data type to another
*/

let numA = '10';
let numB = 12;

/*
	Adding/concatenating a string and a number will result
 as a string
*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);  //String

let coercion1 = numA - numB;
console.log(coercion1); //1012
console.log(typeof coercion);  //String


//Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion);  //Number

//Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF); //1
console.log(typeof numF); //number


//COMPARISON OPERATOR
let juan = "juan";

//Equality Operator (==)
/*
	-Checks whether the operand are equal/have the same content
	-Attempts to CONVERT and COMPARE operands of different data types
	-Returns a boolean value (true / false)


*/

console.log(1 == 1); //T
console.log(1 == 2); //F
console.log(1 == "1"); //T
console.log(1 == true); //T

//compare two strings that are the same
console.log('juan' == 'juan'); //T
console.log('true' == true); //F
console.log(juan == "juan"); //T

//Inequality Operator (!=)

/*
	-Checks whether the operands are not eqial/have different content
	-Attempts to CONVERT AND COMPARE operands of diff data types
*/

console.log(1 != 1);	//F
console.log(1 != 2);	//T
console.log(1 != "1");	//F
console.log(1 != true); //F
console.log("juan" != "juan"); //F
console.log("juan" != juan); //F

//Strict Equality Operator (===)
/*
	-Checks whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values
*/

console.log(1 === 1); //T
console.log(1 === 2); //F
console.log(1 === "1"); //F
console.log(1 === true); //F
console.log("juan" === "juan"); //T
console.log(juan === "juan"); //T


//Strict Inequality Operator (!==)
/*
	Checks whether the operands are not equal/have the same content
	Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //F
console.log(1 !== 2); //T
console.log(1 !== "1"); //T
console.log(1 !== true); //T
console.log("juan" !== "juan"); //F
console.log(juan !== "juan"); //F


//RELATIONAL OPERATOR
//Returns a boolean value
let j = 50;
let k = 65;

//GREATER THAN OPERATOR (>)
let isGreaterThan = j > k; //false
console.log(isGreaterThan);

//LESS THAN OPERATOR (<)
let isLessThan = j < k;
console.log(isLessThan);

//GREATER THAN OR EQUAL OPERATOR (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //false

//LESS THAN OR EQUAL OPERATOR (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
//forced coercion to change the string to a number
console.log(j > numStr);

let str = "thirty";
//(50 is greater than NaN)
console.log(j > str);

//LOGICAL OPERATORS

let isLegalAge = true;
let isRegistered = false;

//Logical AND Operator (&& - Double Ampersand)
//Returns true if all operands are true
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

//Logical OR Operator (|| - Double Pipe)
//Returns true if one of the operands are true
//true || true = true
//true || false = true
//false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

//Logical NOT Operator (! - exclamation point)
//Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);